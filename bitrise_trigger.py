#!/usr/bin/env python
# %%

import argparse
import sys
from time import sleep
from typing import Dict, List, Optional

import requests

BASE_URL = 'https://app.bitrise.io'
API_URL = 'https://api.bitrise.io/v0.1'


STATUS_CODES = {
    0: 'unfinished',
    1: 'success',
    2: 'error',
    3: 'aborted'
}

FINISHED_STATES = [
    'success',
    'error',
    'aborted'
]


class PipelineFailure(Exception):
    def __init__(self, return_code=None, build_id=None):
        self.return_code = return_code
        self.build_id = build_id


def parse_args(args: List[str]):
    parser = argparse.ArgumentParser(
        description='Tool to trigger and monitor a Bitrise workflow',
        add_help=False)
    parser.add_argument('-a', '--api-token', help='personal access token (not required when running detached)')
    parser.add_argument('-b', '--build-token', required=True, help='bitrise build trigger token')
    parser.add_argument('-c', '--commit', help='commit hash to link back to Gitlab repository')
    parser.add_argument('-d', '--detached', action='store_true', default=False)
    parser.add_argument('-e', '--env', action='append')
    parser.add_argument('-h', '--help', action='help', help='show this help message and exit')
    parser.add_argument('-m', '--message', help='build message to describe build in Bitrise')
    parser.add_argument('-s', '--sleep', type=int, default=5)
    parser.add_argument('-r', '--ref', required=True, help='ref to build (branch, tag, commit)')
    parser.add_argument('-w', '--workflow', default='primary', help='workflow name (default: primary)')
    parser.add_argument('app_id')
    return parser.parse_args(args)


def parse_env(env: List[str]) -> List[Dict]:
    res = []
    for e in env:
        k, v = e.split('=')
        res.append(
            dict(mapped_to=k, value=v, is_expand=True)
        )
    return res


def start_build(
    build_token: str,
    app_id: str,
    ref: str,
    workflow: Optional[str]=None,
    envs: List[Dict]=[],
    commit: Optional[str]=None,
    message: Optional[str]=None
) -> str:
    """
    Starts a build and returns the build id.
    https://devcenter.bitrise.io/api/build-trigger/
    """
    build_params = dict(branch=ref, environments=envs)
    if workflow:
        build_params['workflow_id'] = workflow
    if commit:
        build_params['commit_hash'] = commit
    if message:
        build_params['commit_message'] = message
    r = requests.post(
        f'{BASE_URL}/app/{app_id}/build/start.json',
        json=dict(
            hook_info=dict(
                type="bitrise",
                build_trigger_token=build_token
            ),
            build_params=build_params,
            triggered_by="bitrise-trigger"
        )
    )
    assert r.status_code == 201, f'expected status code 201, was {r.status_code}, response: {r.text}'
    # r.json() -> {'status': 'ok', 'message': 'webhook processed', 'slug': 'a1c82fc91e4dfe93', 'service': 'bitrise', 'build_slug': '44d49adbf663610f', 'build_number': 8, 'build_url': 'https://app.bitrise.io/build/44d49adbf663610f', 'triggered_workflow': 'primary'}
    data = r.json()
    status = data.get('status')
    message = data.get('message')
    assert status == 'ok', f'starting build failed - status: {status}, message: {message}'
    return data['build_slug'], data.get('build_url')


def get_build_status(api_token: str, app_id: str, build_id: str):
    r = requests.get(
        f'{API_URL}/apps/{app_id}/builds/{build_id}',
        headers={
            'Authorization': api_token
        }
    )
    assert r.status_code == 200, f'expected status code 200, was {r.status_code}, response: {r.text}'
    data = r.json()['data']
    status = data['status']
    return STATUS_CODES[status]


def trigger(args: List[str]) -> int:
    args = parse_args(args)

    assert args.app_id, 'app id must be set'
    assert args.build_token, 'bitrise build trigger token must be set'
    assert args.ref, 'must provide build ref'
    assert args.sleep > 0, 'sleep parameter must be > 0'

    app_id = args.app_id
    build_token = args.build_token
    ref = args.ref
    workflow = args.workflow
    envs = parse_env(args.env or [])

    print(f"Triggering build for ref '{ref}' and app id {app_id}")
    build_id, build_url = start_build(build_token=build_token, app_id=app_id, ref=ref, workflow=workflow, envs=envs, commit=args.commit, message=args.message)

    assert build_id, 'must have a build id'

    print(f"Started build {build_id}: {build_url}")

    if args.detached:
        print('Detached mode: not monitoring build status - exiting now.')
        return build_id

    print(f"Waiting for build {build_id} to finish ...")

    status = None
    max_retries = 5
    retries_left = max_retries

    while status not in FINISHED_STATES:
        try:
            assert args.api_token is not None, 'status checks require an api token (-a parameter missing)'
            status = get_build_status(
                api_token=args.api_token,
                app_id=app_id,
                build_id=build_id
            )
            # reset retries_left if the status call succeeded (fail only on consecutive failures)
            retries_left = max_retries
        except Exception as e:
            print(f'Polling for status failed: {e}')
            if retries_left == 0:
                print(f'Polling failed {max_retries} consecutive times.')
                print('check your api token, or check if there are connection issues.')
                print()
                raise PipelineFailure(return_code=2, build_id=build_id)
            retries_left -= 1

        print('.', end='', flush=True)
        sleep(args.sleep)

    print()

    if status == 'success':
        print('Build succeeded')
        return build_id
    else:
        raise PipelineFailure(return_code=1, build_id=build_id)


# %%


if __name__ == "__main__":
    try:
        trigger(sys.argv[1:])
        sys.exit(0)
    except PipelineFailure as e:
        sys.exit(e.return_code)
