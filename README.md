[![pipeline-status](https://gitlab.com/finestructure/bitrise-trigger/badges/master/build.svg)](https://gitlab.com/finestructure/bitrise-trigger/pipelines)
[![Twitter](https://img.shields.io/badge/twitter-@__sa__s-blue.svg?logo=twitter)](https://twitter.com/_sa_s)

# Bitrise-trigger

Bitrise-trigger is a ready-made container that allows triggering and waiting for bitrise workflows from GitLab CI pipelines.

You can use it as a building block in GitLab pipelines to run build stages on Bitrise, for instance Android or iOS tests.

## Why not just use Bitrise’s Gitlab integration?
Bitrise is very well integrated with Gitlab. In fact, this project was born out of that integration when we came across one bottle-neck we needed to fix: Running pipelines in steps for parallelism and retries.

While this is something Gitlab supports, when you connect a project to Bitrise you do not get to choose different workflows to trigger. You essentially delegate your Gitlab stages to Bitrise, which then run in full.

Since our pipelines are long and we don’t want to run them sequentially – or have to run the whole pipeline again if a single step fails – this is something we desperately needed.

And this why Bitrise trigger exists. It is a sibling to [pipeline-trigger](https://gitlab.com/finestructure/pipeline-trigger), which is a generic way of triggering Gitlab pipelines across projects. Both these projects together can be used to span projects and Vendors to run complex CI pipelines. (And this should be easy to extend to other vendors with web APIs.)

## Examples

### Tests in this repository

The CI pipeline for this very project can serve as an example of how to use bitrise-trigger – its tests triggering a bitrise pipeline and checks the results.

In the following pipeline image, `test basic` and `test detached` are two bitrise workflows being triggered:

![bitrise-trigger pipeline](/uploads/7def481dc8f04c912e44b1d86c1e0055/FullSizeRender.jpg)

The definition of the first job, `test basic`, (see this project’s `.gitlab-ci.yml`) looks as follows:

```
image: registry.gitlab.com/finestructure/bitrise-trigger:1.0.0

test basic:
  stage: test
  script: |
    bitrise-trigger \
      --api-token="$API_TOKEN" \
      --build-token="$BUILD_TOKEN" \
      -r "$CI_COMMIT_REF_NAME" \
      -e foo=bar \
      -e other=ignored \
      -w primary \
      -c $CI_COMMIT_SHA \
      -m "triggered by `$CI_JOB_NAME/$REF` $CI_JOB_URL" \
      "$APP_ID"
```

Using the bitrise-trigger image provides `bitrise-trigger` as a script command. `bitrise-trigger` takes a few arguments to make the connection to your Bitrise project:

- `--api-token` is your Bitrise token to access the API. API access is required to check for pipeline results. Bitrise trigger can be run in ‘detached’ mode, in which case it will only trigger the Bitrise pipeline and not poll for the result. In this mode the API token is not required.
- `-r` sets the reference (branch, tag, sha) to build on Bitrise
- You can provide environment variables to your Bitrise workflows by passing `-e` parameters. Environment variables can be useful if your Bitrise workflows need some configuration, for instance to set debug or release builds. This avoids having to set up different workflows for different build flavours.
- `-w` sets the workflow you want to trigger in Bitrise.
- `-c` is the commit sha you are building. Setting this is useful to display the revision in the Bitrise console (see the screenshot below).
- Similarly, use `-m` to set a build description to make it easier to identify the build in the Bitrise console.
- Finally, pass in your Bitrise `APP_ID` (required parameter).

The following screenshot shows what this workflow looks like in the Bitrise console:

![Bitrise console](/uploads/b98699ea91e56d51067490667f344c1d/Screen_Shot_2018-09-26_at_10.26.24.png)

### Triggering a set of Android tests

The following screenshot shows a set of Android workflows being run on Bitrise:

![bitrise-trigger Android pipeline](/uploads/3389615e6ded5d59244805aa34bccb70/FullSizeRender.jpg)

The corresponding `.gitlab-ci.yml` looks as follows:

```
image: registry.gitlab.com/finestructure/bitrise-trigger:1.0.0


variables:
  REF: $CI_COMMIT_REF_NAME
  # set via secret variables
  APP_ID: $BITRISE_APP_ID
  API_TOKEN: $BITRISE_API_TOKEN
  BUILD_TOKEN: $BITRISE_BUILD_TRIGGER_TOKEN

stages:
  - build
  - test

.defaults: &defaults
  variables:
    WORKFLOW: $CI_JOB_NAME
  script: |
    ENV=${ENV:-dev}
    echo REF: $REF
    echo ENV: $ENV
    echo WORKFLOW: $WORKFLOW
    bitrise-trigger \
      --api-token="$API_TOKEN" \
      --build-token="$BUILD_TOKEN" \
      --ref="$REF" \
      --env=ENV=$ENV \
      --workflow="$WORKFLOW" \
      --commit="$CI_COMMIT_SHA" \
      -m "${REF}⇄${ENV} → $CI_JOB_URL" \
      "$APP_ID"

build:
  stage: build
  <<: *defaults

unit-tests:
  stage: test
  <<: *defaults

ui-tests-mock:
  stage: test
  <<: *defaults

ui-tests-env:
  stage: test
  <<: *defaults
```

What we’re doing is triggering bitrise workflows with the same name as our GitLab jobs by passing `CI_JOB_NAME` as the `--workflow=` parameter.

This makes it easy to use Gitlab as the driver for the stages and allows you to run jobs concurrently if your Bitrise plan allows it.
